def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, length = 2)
  (1..length).to_a.collect{ string }.join(' ')
end

def start_of_word(string, length = 1)
  string[0, length]
end

def first_word(string)
  string.split[0]
end

def titleize(string)
  string.words.with_index{ |word, i| word.titleize(i) }.join(' ')
end

class String

  def titleize(index = 0)
    if little_word? && index != 0
      self
    else
      self.capitalize
    end
  end

  def words
    self.split.map
  end

  private

  def little_word?
    ['a', 'and', 'over', 'the'].include?(self)
  end

end
