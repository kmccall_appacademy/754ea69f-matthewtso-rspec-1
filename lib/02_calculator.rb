def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def sum(array)
  array.reduce(0, :+)
end

def multiply(a, b)
  a * b
end

def divide(a, b)
  a / b
end

def power(a, b)
  a ** b
end

def factorial(number)
  if number == 1
    1
  else
    factorial(number - 1) * number
  end
end
