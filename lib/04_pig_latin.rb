
def translate(sentence)
  sentence.split.map{ |word| translate_word(word) }.join(' ')
end

def translate_word(word)
  word.get_rest + word.get_phoneme + 'ay'
end

def is_vowel?(character)
  ['a', 'e', 'i', 'o', 'u'].include?(character)
end

class String

  def begins_with_vowel?
    ['a', 'e', 'i', 'o', 'u'].include?(self[0])
  end

  def phoneme_index
    if has_qu?
      qu_index
    else
      i = 0
      begin
        if is_vowel?(self[i])
          break
        end
        i += 1;
      end while i < 3
      i
    end
  end

  # returns phoneme and leaves self with the rest
  def get_phoneme
    self[0, phoneme_index]
  end

  def get_rest
    self[phoneme_index, self.length]
  end

  private

  def has_qu?
    self[0, 2] == 'qu' || self[1, 2] == 'qu'
  end

  def qu_index
    self[0, 2] == 'qu' ? 2 : 3
  end

end
