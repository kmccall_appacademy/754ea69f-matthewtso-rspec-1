def ftoc(fahrenheit)
  (fahrenheit.to_f - 32) * 5 / 9
end

def ctof(celcius)
  (celcius.to_f / 5 * 9) + 32
end
